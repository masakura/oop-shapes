import Canvas from './lib/canvas.js';
import Circle from './lib/circle.js';

const canvas = new Canvas(document.getElementById('canvas'));

canvas.draw(new Circle());
