class Shape {
    /**
     * @param {Drawing} drawing
     */
    draw(drawing) {
        throw new Error('Require implement');
    }
}

export default Shape;