import Shape from './shape.js';

class Circle extends Shape {
    draw(drawing) {
        drawing.ellipse(0, 0, 50, 50);
    }
}

export default Circle;