const privates = Symbol('privates');

class Drawing {
    constructor(element) {
        this[privates] = {
            context: element.getContext('2d'),
        };
    }

    ellipse(x1, y1, x2, y2) {
        /** @type {CanvasRenderingContext2D} */
        const context = this[privates].context;
        console.log(context.strokeStyle);

        const center = {
            x: (x2 - x1) / 2 + x1,
            y: (y2 - y1) / 2 + y1,
        };
        const r = {
            x: (x2 - x1) / 2,
            y: (y2 - y1) / 2,
        };

        context.beginPath();
        context.ellipse(center.x, center.y, r.x, r.y, 0, 0, Math.PI * 2);
        context.stroke();
    }
}

export default Drawing;