import Drawing from './drawing.js';

const privates = Symbol('privates');

class Canvas {
    constructor(element) {
        this[privates] = {
            drawing: new Drawing(element),
        };
    }

    /**
     * @param {Shape} shape
     */
    draw(shape) {
        shape.draw(this[privates].drawing);
    }
}

export default Canvas;